package runner;

import org.testng.annotations.Test;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@CucumberOptions(
        features={"src//test//java//features"},
        glue={"stepsDefinitions","utilities"},
        tags ={"@IntroKwai"},
        plugin = {"junit:target/reports/junit.xml", "pretty", "html:target/cucumber","json:target/cucumber.json"}
)
@Test
public class RunTest extends AbstractTestNGCucumberTests {

}