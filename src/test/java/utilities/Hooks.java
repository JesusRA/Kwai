package utilities;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Driver;
import java.util.concurrent.TimeUnit;

public class Hooks {
    public static String ANDROID_PACKAGE = "com.kwai.video";

    private static AndroidDriver driver;
    private static Wait<AndroidDriver> wait;

    //DISPOSITIVO FISICO
    /*@Before()
    public void setUp() throws MalformedURLException {
        System.out.println("APLICACION INICIANDO...");
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("deviceName", "HUAWEI VNS-AL00");
        cap.setCapability("udid","3DN0216510002503");
        cap.setCapability("platformName","Android");
        cap.setCapability("platformVersion","6.0");
        cap.setCapability("appPackage", ANDROID_PACKAGE);
        cap.setCapability("appActivity","com.yxcorp.gifshow.homepage.HomeActivity");
        cap.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS,true);
        cap.setCapability(MobileCapabilityType.NO_RESET,true);
        cap.setCapability(MobileCapabilityType.FULL_RESET,false);
        URL url = new URL("http://localhost:4723/wd/hub");
        //AndroidDriver<WebElement> driver = new AndroidDriver<WebElement>(url, cap);
        driver = new AndroidDriver<MobileElement>(url, cap);
        System.out.println("APLICACION SINCRO INICIADA EXITOSAMENTE");

    }*/
    //EMULADOR
    @Before()
    public void setUp() throws MalformedURLException {
        System.out.println("APLICACION INICIANDO...");
        DesiredCapabilities cap2 = new DesiredCapabilities();
        cap2.setCapability("deviceName", "Android SDK built for x86");
        cap2.setCapability("udid", "emulator-5554");
        cap2.setCapability("platformName","Android");
        cap2.setCapability("platformVersion","8.1.0");
        cap2.setCapability("appPackage", ANDROID_PACKAGE);
        cap2.setCapability("appActivity","com.yxcorp.gifshow.homepage.HomeActivity");
        cap2.setCapability(MobileCapabilityType.NO_RESET,true);
        cap2.setCapability(MobileCapabilityType.FULL_RESET,false);
        cap2.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS,true);
        URL url = new URL("http://localhost:4723/wd/hub");
        driver = new AndroidDriver<MobileElement>(url, cap2);
        System.out.println("APLICACION SINCRO INICIADA EXITOSAMENTE");

    }
    @After
    public void tearDown()
    {
        driver.quit();
    }
    public static AndroidDriver getDriver()
    {
        return driver;
    }
    public static Wait<AndroidDriver> getWait() {
        if (wait == null) {
            wait = new FluentWait<AndroidDriver>(driver).
                    withTimeout(10, TimeUnit.SECONDS)
                    .pollingEvery(5, TimeUnit.MILLISECONDS);
        }
        return wait;
    }
}
