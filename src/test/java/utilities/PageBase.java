package utilities;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import io.appium.java_client.TouchAction;


import java.time.Duration;
import java.util.List;

public class PageBase {

    public AndroidDriver driver;
    public Wait<AndroidDriver> wait;

    public PageBase() {
        this.driver = Hooks.getDriver();
        this.wait = Hooks.getWait();
    }

    public void completeFieldById(String locator, String texto) throws Exception {
        waitForElementById(locator);
        WebElement webElement = driver.findElement(By.id(locator));
        //WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(locator))));
        webElement.sendKeys(texto);
    }

    public void clickOnElement(WebElement webElement) {
        webElement.click();
    }

    public void clickOnElementById(String locator) throws Exception {
        waitForElementById(locator);
        WebElement webElement = driver.findElement(By.id(locator));
        //WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(locator))));
        webElement.click();
    }

    public void clickOnElementByXpath(String locator) throws Exception {
        waitForElementByXpath(locator);
        WebElement webElement = driver.findElement(By.xpath(locator));
        //WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
        webElement.click();
    }

    public void clickOnElementByUiAut(String locator) throws Exception {
        waitForElementByUIAut(locator);
        WebElement webElement = driver.findElementByAndroidUIAutomator(locator);
        //WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByAndroidUIAutomator(locator)));
        webElement.click();
    }


    public void clickOnElementByAccesiId(String locator) throws Exception {
        waitForElementByAccessibilityId(locator);
        WebElement webElement = driver.findElementByAccessibilityId(locator);
        webElement.click();

    }

    public void waitForElementByAccessibilityId(String locator) throws Exception {
        waitForElementBy(new MobileBy.ByAccessibilityId(locator));
    }

    public void elemento_visible(String locator) throws Exception {
        waitForElementById(locator);
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
    }

    public void waitElementNotVisibleById(String locator) throws Exception {
        waitForNotVisibleElementBy(By.id(locator));
    }

    public void elemento_visibleByXapath(String locator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
    }

    public String mostrar_texto_contenido(String locator) {
        String text = driver.findElement(By.id(locator)).getText();
        Assert.assertFalse(text.isEmpty());
        return text;
    }

    public String mostrar_texto_contenidoByxapth(String locator) {
        String text = driver.findElement(By.xpath(locator)).getText();
        Assert.assertFalse(text.isEmpty());
        return text;
    }

    public List<WebElement> getElementsById(String locator) {
        return driver.findElements(By.id(locator));
    }

    public List<WebElement> getElementsByXpath(String locator) {
        return driver.findElements(By.xpath(locator));
    }

    public List<WebElement> getElementsByClass(String locator) {
        return driver.findElements(By.className(locator));
    }

    protected void scrolling(int startX, int startY, int endX, int endY) {
        new TouchAction((PerformsTouchActions) driver)
                .press(PointOption.point(startX, startY))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
                .moveTo(PointOption.point(endX, endY))
                .release().perform();
    }

    public void goBack() {
        driver.navigate().back();
    }

    public void sleep(int segundos) throws InterruptedException {
        Thread.sleep(segundos * 1000);
    }

    public void waitForElementById(String locator) throws Exception {
        waitForElementBy(By.id(locator));
    }

    public void waitForElementByXpath(String locator) throws Exception {
        waitForElementBy(By.xpath(locator));
    }

    public void waitForNotVisibleElementBy(By locator) throws Exception {
        for (int i = 0; i < 121; i++) {
            List<WebElement> elementsList = driver.findElements(locator);
            if (elementsList.size() == 0) {
                break;
            } else {
                Thread.sleep(500);
            }
        }
    }

    public void waitForElementBy(By locator) throws Exception {
        for (int i = 0; i < 21; i++) {
            List<WebElement> elementsList = driver.findElements(locator);
            if (elementsList.size() > 0) {
                break;
            } else {
                Thread.sleep(500);
            }
        }
    }

    public void waitForElementByUIAut(String locator) throws Exception {
        for (int i = 0; i < 21; i++) {
            List<WebElement> elementsList = driver.findElementsByAndroidUIAutomator(locator);
            if (elementsList.size() > 0) {
                break;
            } else {
                Thread.sleep(500);
            }
        }
    }
}
