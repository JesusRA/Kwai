package stepsDefinitions;

import cucumber.api.java.en.Given;
import pages.KwaiPage;

public class KwaiIngresoSteps {

    KwaiPage kwaiPage = new KwaiPage();

    @Given("^Usuario se loguea en Kwai$")
    public void usuario_Kwai() throws Exception {
        kwaiPage.loginKwai();
    }
}

